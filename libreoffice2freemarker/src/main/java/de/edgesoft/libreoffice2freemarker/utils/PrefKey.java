
package de.edgesoft.libreoffice2freemarker.utils;

/**
 * Preference keys.
 *
 * For enums I use the coding style of jaxb, so there will be no inconsistencies.
 *
 * ## Legal stuff
 *
 * Copyright 2018-2018 Ekkart Kleinod <ekleinod@edgesoft.de>
 *
 * The program is distributed under the terms of the GNU General Public License.
 *
 * See COPYING for details.
 *
 * This file is part of LibreOffice 2 Freemarker.
 *
 * LibreOffice 2 Freemarker is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * LibreOffice 2 Freemarker is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with LibreOffice 2 Freemarker. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Ekkart Kleinod
 * @version 1.0.0
 * @since 1.0.0
 */
public enum PrefKey {

	LO_FILE,

	OUTFILE,

	TEMPLATE,
	;

	private final String value;

	PrefKey() {
		value = name().toLowerCase();
	}

	public String value() {
			return value;
	}

	public static PrefKey fromValue(String v) {
		for (PrefKey c: PrefKey.values()) {
			if (c.value.equals(v)) {
				return c;
			}
		}
		throw new IllegalArgumentException(v);
	}

}

/* EOF */
