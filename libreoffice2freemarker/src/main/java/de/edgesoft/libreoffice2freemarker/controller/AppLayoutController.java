package de.edgesoft.libreoffice2freemarker.controller;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.odftoolkit.simple.SpreadsheetDocument;
import org.odftoolkit.simple.table.Row;
import org.odftoolkit.simple.table.Table;

import de.edgesoft.libreoffice2freemarker.utils.PrefKey;
import de.edgesoft.libreoffice2freemarker.utils.Prefs;
import de.edgesoft.libreoffice2freemarker.utils.Resources;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import freemarker.template.TemplateExceptionHandler;
import javafx.application.Platform;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.scene.Cursor;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

/**
 * Controller for application layout.
 *
 * ## Legal stuff
 *
 * Copyright 2018-2018 Ekkart Kleinod <ekleinod@edgesoft.de>
 *
 * The program is distributed under the terms of the GNU General Public License.
 *
 * See COPYING for details.
 *
 * This file is part of LibreOffice 2 Freemarker.
 *
 * LibreOffice 2 Freemarker is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * LibreOffice 2 Freemarker is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with LibreOffice 2 Freemarker. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Ekkart Kleinod
 * @version 1.0.0
 * @since 1.0.0
 */
public class AppLayoutController {

	/**
	 * Application icon.
	 */
	public static final Image ICON = Resources.loadImage("images/icon-32.png");

	/**
	 * Content ID filename.
	 */
	public static final String CONTENT_FILENAME = "filename";

	/**
	 * Content ID sheet.
	 */
	public static final String CONTENT_SHEETS = "sheets";

	/**
	 * Content ID name.
	 */
	public static final String CONTENT_NAME = "name";

	/**
	 * Content ID heading.
	 */
	public static final String CONTENT_HEADINGS = "headings";

	/**
	 * Content ID rows.
	 */
	public static final String CONTENT_ROWS = "rows";

	/**
	 * Heading row.
	 */
	public static final int ROW_HEADING = 0;

	/**
	 * App border pane.
	 */
	@FXML
	private BorderPane appPane;

	/**
	 * Menu item program -> quit.
	 */
	@FXML
	private MenuItem mnuProgramQuit;

	/**
	 * LibreOffice file.
	 */
	@FXML
	private TextField txtLibreOffice;

	/**
	 * Template file.
	 */
	@FXML
	private TextField txtTemplate;

	/**
	 * Output file.
	 */
	@FXML
	private TextField txtOutFile;


	/**
	 * Text area for log.
	 */
	@FXML
	private TextArea txtLog;


	/**
	 * Create button.
	 */
	@FXML
	private Button btnCreateFile;


	/**
	 * Primary stage (window).
	 */
	private Stage window = null;


	/**
	 * Busy property.
	 */
	private BooleanProperty propBusy = new SimpleBooleanProperty(false);


	/**
	 * Initializes the controller class.
	 *
	 * This method is automatically called after the fxml file has been loaded.
	 */
	@FXML
	private void initialize() {

		// icons
		mnuProgramQuit.setGraphic(new ImageView(Resources.loadImage("icons/24x24/actions/application-exit.png")));

	}

	/**
	 * Initializes the controller with things, that cannot be done during {@link #initialize()}.
	 *
	 * @param thePrimaryStage primary stage
	 */
	public void initController(final Stage thePrimaryStage) {

		window = thePrimaryStage;

		// set icon
		window.getIcons().add(ICON);

		// Show the scene containing the root layout.
		Scene scene = new Scene(appPane);
		window.setScene(scene);
		window.show();

		// load last values
		txtLibreOffice.setText(Prefs.get(PrefKey.LO_FILE));
		txtTemplate.setText(Prefs.get(PrefKey.TEMPLATE));
		txtOutFile.setText(Prefs.get(PrefKey.OUTFILE));

		// set handler for close requests (x-button of window)
		window.setOnCloseRequest(event -> {
			event.consume();
			handleProgramExit();
		});

		// button enabling
		btnCreateFile.disableProperty().bind(
				txtLibreOffice.textProperty().isEmpty()
				.or(txtTemplate.textProperty().isEmpty())
				.or(txtOutFile.textProperty().isEmpty())
				.or(propBusy)
				);

	}

	/**
	 * Program menu exit.
	 */
	@FXML
	public void handleProgramExit() {

		Prefs.put(PrefKey.LO_FILE, txtLibreOffice.getText());
		Prefs.put(PrefKey.TEMPLATE, txtTemplate.getText());
		Prefs.put(PrefKey.OUTFILE, txtOutFile.getText());

		Platform.exit();
	}

	/**
	 * Create file.
	 */
	@FXML
	private void handleCreateFile() {

		propBusy.setValue(true);
		window.getScene().setCursor(Cursor.WAIT);
		txtLog.clear();

		Task<Void> taskCreate = new Task<Void>() {
			@Override protected Void call() throws Exception {

				// read data
				Path pathLOFile = Paths.get(txtLibreOffice.getText()).toAbsolutePath();
				appendTextLogMessage(MessageFormat.format("Lade Daten aus ''{0}''.", pathLOFile.toString()));

				Map<String, Object> theContent = readFromODS(pathLOFile);

				if ((theContent == null) || theContent.isEmpty()) {
					appendTextLogMessage("Keine Daten vorhanden.");
					return null;
				}

				appendTextLogMessage(MessageFormat.format("Daten aus ''{0}'' geladen.", pathLOFile.toString()));


				// read template
				Path pathTemplate = Paths.get(txtTemplate.getText()).toAbsolutePath();
				appendTextLogMessage(MessageFormat.format("Lade Template ''{0}''.", pathTemplate.toString()));

				Template tplDocument = readTemplate(pathTemplate);

				if (tplDocument == null) {
					appendTextLogMessage("Template konnte nicht geladen werden.");
					return null;
				}

				appendTextLogMessage(MessageFormat.format("Template ''{0}'' geladen.", pathTemplate.toString()));


				// generate file
				Path pathOutFile = Paths.get(txtOutFile.getText()).toAbsolutePath();
				appendTextLogMessage(MessageFormat.format("Generiere Datei ''{0}''.", pathOutFile.toString()));

				if (!processTemplate(theContent, tplDocument, pathOutFile)) {
					appendTextLogMessage("Datei konnte nicht generiert werden.");
					return null;
				}

				appendTextLogMessage(MessageFormat.format("Datei ''{0}'' generiert.", pathOutFile.toString()));


				return null;
			}
		};

        // task succeeded - show results
        taskCreate.setOnSucceeded(event -> {

        	appendTextLogMessage("Fertig.");
        	propBusy.setValue(false);
        	window.getScene().setCursor(Cursor.DEFAULT);

        });

        Thread thread = new Thread(taskCreate);
        thread.start();

	}

	/**
	 * Updates text log.
	 *
	 * @param message message to append
	 */
	private void appendTextLogMessage(String message) {
		if (Platform.isFxApplicationThread()) {
			txtLog.appendText(String.format("%s%n", message));
		} else {
			Platform.runLater(() -> txtLog.appendText(String.format("%s%n", message)));
		}
	}

	/**
	 * Process template and save output.
	 *
	 * @param theContent content
	 * @param theTemplate template
	 * @param theOutFilePath output file path
	 * @return success
	 */
	private boolean processTemplate(final Map<String, Object> theContent, final Template theTemplate, final Path theOutFilePath) {

		try (Writer wrtFile = new OutputStreamWriter(new FileOutputStream(theOutFilePath.toFile()), StandardCharsets.UTF_8)) {

			theTemplate.process(theContent, wrtFile);

		} catch (TemplateException | IOException e) {

			e.printStackTrace();
			appendTextLogMessage(String.format("  %s", e.getMessage()));
			return false;

		}

		return true;

	}

	/**
	 * Read template.
	 *
	 * @param theTemplatePath template path
	 * @return template
	 */
	private Template readTemplate(final Path theTemplatePath) {

		Template tplDocument = null;

		try {

			Configuration tplConfig = new Configuration(Configuration.VERSION_2_3_26);
			tplConfig.setDefaultEncoding(StandardCharsets.UTF_8.name());
			tplConfig.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);
			tplConfig.setLogTemplateExceptions(false);

			tplConfig.setDirectoryForTemplateLoading(theTemplatePath.getParent().toFile());
			tplDocument = tplConfig.getTemplate(theTemplatePath.getFileName().toString());

		} catch (IOException | NullPointerException e) {
			e.printStackTrace();
			appendTextLogMessage(String.format("  %s", e.getMessage()));
			return null;
		}

		return tplDocument;

	}

	/**
	 * Read content from ods file.
	 *
	 * @param theLOFilePath input path
	 * @return content
	 */
	private Map<String, Object> readFromODS(final Path theLOFilePath) {

		if (!theLOFilePath.toString().endsWith(".ods")) {
			appendTextLogMessage("Unbekannte Dateiendung.");
			return null;
		}

		Map<String, Object> mapContent = new HashMap<>();

		mapContent.put(CONTENT_FILENAME, theLOFilePath.toString());

		try {

			// load doc
			SpreadsheetDocument theDoc = SpreadsheetDocument.loadDocument(theLOFilePath.toFile());

			List<Map<String, Object>> lstSheets = new ArrayList<>();

			// cycle through sheets
			for (int iSheet = 0; (iSheet < theDoc.getSheetCount()); iSheet++) {

				Map<String, Object> mapSheet = readSheet(theDoc.getSheetByIndex(iSheet));
				if ((mapSheet != null) && !mapSheet.isEmpty()) {

					lstSheets.add(mapSheet);

					appendTextLogMessage(MessageFormat.format("  Tabellenblatt ''{0}'' zugefügt.",
							mapSheet.get(CONTENT_NAME)
							));

				}

			}

			mapContent.put(CONTENT_SHEETS, lstSheets);

			theDoc.close();

		} catch (Exception e) {

			e.printStackTrace();
			appendTextLogMessage(String.format("  %s", e.getMessage()));
			return null;

		}

		return mapContent;

	}

	/**
	 * Read content from sheet.
	 *
	 * @param theSheet sheet
	 * @return content
	 */
	private Map<String, Object> readSheet(final Table theSheet) {

		appendTextLogMessage(MessageFormat.format("  Tabellenblatt ''{0}'' in Arbeit...",
				theSheet.getTableName()
				));

		// no data in first row - stop processing
		if (isRowEmpty(theSheet.getRowByIndex(ROW_HEADING))) {
			appendTextLogMessage(MessageFormat.format("  Tabellenblatt ''{0}'' leer, wird übersprungen.",
					theSheet.getTableName()
					));
			return null;
		}

		// store sheet data in hashmap, beginning with name, store in content
		Map<String, Object> mapSheet = new HashMap<>();
		mapSheet.put(CONTENT_NAME, theSheet.getTableName());

		// read headings, use for map
		Map<Integer, String> mapHeading = new HashMap<>();
		Row rowHeading = theSheet.getRowByIndex(ROW_HEADING);
		for (int iCell = 0; (!rowHeading.getCellByIndex(iCell).getDisplayText().isEmpty()); iCell++) {
			mapHeading.put(iCell, rowHeading.getCellByIndex(iCell).getDisplayText());
		}

		mapSheet.put(CONTENT_HEADINGS, mapHeading);

		// process rows
		List<Map<String, String>> lstRows = new ArrayList<>();
		mapSheet.put(CONTENT_ROWS, lstRows);
		for (int iRow = 1; (!isRowEmpty(theSheet.getRowByIndex(iRow))); iRow++) {

			Row theRow = theSheet.getRowByIndex(iRow);
			Map<String, String> mapRow = new HashMap<>();

			for (Entry<Integer, String> theHeading : mapHeading.entrySet()) {
				mapRow.put(theHeading.getValue(), theRow.getCellByIndex(theHeading.getKey()).getDisplayText());
			}

			lstRows.add(mapRow);

			appendTextLogMessage(MessageFormat.format("    Zeile ''{0,number}'' bearbeitet.",
					iRow
					));

		}

		return mapSheet;

	}

	/**
	 * Is row empty row?
	 *
	 * There is no good method for computing empty rows, thus all cells are looked at.
	 *
	 * @todo check performance of this method in case of empty rows
	 *
	 * @param theRow row
	 * @return is row empty?
	 */
	private boolean isRowEmpty(final Row theRow) {

		for (int iColumn = 0; iColumn < theRow.getCellCount(); iColumn++) {
			if (!theRow.getCellByIndex(iColumn).getDisplayText().isEmpty()) {
				return false;
			}
		}

		return true;

	}

}

/* EOF */
