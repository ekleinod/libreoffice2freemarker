package de.edgesoft.libreoffice2freemarker;

import java.util.Map;

import de.edgesoft.libreoffice2freemarker.controller.AppLayoutController;
import de.edgesoft.libreoffice2freemarker.utils.Resources;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

/**
 * LibreOffice2FreeMarker application.
 *
 * ## Legal stuff
 *
 * Copyright 2018-2018 Ekkart Kleinod <ekleinod@edgesoft.de>
 *
 * The program is distributed under the terms of the GNU General Public License.
 *
 * See COPYING for details.
 *
 * This file is part of LibreOffice 2 Freemarker.
 *
 * LibreOffice 2 Freemarker is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * LibreOffice 2 Freemarker is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with LibreOffice 2 Freemarker. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Ekkart Kleinod
 * @version 1.0.0
 * @since 1.0.0
 */
public class LibreOffice2FreeMarker extends Application {

	/**
	 * Starts the application.
	 *
	 * @param args command line arguments
	 */
	public static void main(String[] args) {
		launch(args);
	}

	/**
	 * The main entry point for all JavaFX applications.
	 * The start method is called after the init method has returned,
	 * and after the system is ready for the application to begin running.
	 *
	 * @param window window (primary stage)
	 */
	@Override
	public void start(Stage window) {

		// load app layout and controller, then delegate control to controller
		Map.Entry<Pane, FXMLLoader> pneLoad = Resources.loadPane("AppLayout");
		((AppLayoutController) pneLoad.getValue().getController()).initController(window);

	}

}

/* EOF */
