# LibreOffice 2 Freemarker

This program reads the content of a LibreOffice file and uses Freemarker for output of the content.

## Git-Repository

The branching model regards to the stable mainline model described in <http://www.bitsnbites.eu/a-stable-mainline-branching-model-for-git>

This means, there is always a stable mainline, the `master` branch.
This branch ist always compileable and testable, both without errors.

Feature are developed using `feature` branches, named `feature/t<ticket number>-<description>`, e.g. `feature/t43-stable_mainline`

Releases are created as branches of the mainline: `release/v<major>.<minor>`, e.g. `release/v0.15`
Additionally, each release is tagged.
Patches are made in the according release branch.
Minor version changes get their own release branch.

## Legal stuff

### Licenses

License of the programs: GNU General Public License.
See file [COPYING](COPYING).

### Copyright

Copyright 2018-2018 Ekkart Kleinod <ekleinod@edgesoft.de>

The program is distributed under the terms of the GNU General Public License.

See [COPYING](COPYING) for details.

This file is part of LibreOffice 2 Freemarker.

LibreOffice 2 Freemarker is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LibreOffice 2 Freemarker is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LibreOffice 2 Freemarker. If not, see <http://www.gnu.org/licenses/>.
